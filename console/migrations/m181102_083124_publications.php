<?php

use yii\db\Migration;

/**
 * Class m181102_083124_publications
 */
class m181102_083124_publications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%publications}}', [
            'id' => $this->primaryKey(),
            'twitter_publication_id' => $this->string()->unique(),
            'user' => $this->string(),
            'tweet' => $this->text(),
            'hashtag' => $this->text(),
            'created_at' => $this->integer(),

        ], $tableOptions);


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%publications}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181102_083124_publications cannot be reverted.\n";

        return false;
    }
    */
}
