<?php

namespace frontend\controllers;

use frontend\models\Publications;
use frontend\models\UserList;
use yii\base\Security;
use yii\helpers\VarDumper;

class ApiController extends \yii\web\Controller
{
    public function actionAdd($id = null,$user = null,$secret = null)
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        try{
            $userList = new UserList();

            if($id == null || $user == null || $secret == null) return ['error'=> 'missing parameter'];
            if(!UserList::checkSecret($id,$secret,$user)) return ['error'=> 'access denied'];

            $userList->twitter_user_name = $user;

            if($userList->save()) return '';

        }catch (\Exception $e ){
            return ['error'=> 'internal error'];
        }
    }


    public function actionFeed($id = null,$secret = null)
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        try{
            if($id == null || $secret == null) return ['error'=> 'missing parameter'];
            if(!UserList::checkSecret($id,$secret)) return ['error'=> 'access denied'];

            $publications = Publications::find()->all();
            $response = [];
            foreach ($publications as $publication){
                $response['feed'][] = [
                    'user'=>$publication->user,
                    'tweet'=>$publication->tweet,
                    'hashtag'=>explode('; ',$publication->hashtag),
                ];
            }

            return $response??'';

        }catch (\Exception $e ){
            return ['error'=> 'internal error'];
        }
    }

    public function actionRemove($id = null,$user = null,$secret = null)
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        try{
            if($id == null || $user == null || $secret == null) return ['error'=> 'missing parameter'];
            if(!UserList::checkSecret($id,$secret,$user)) return ['error'=> 'access denied'];

            $userModel = UserList::findOne(['twitter_user_name'=>$user]);

            if(!empty($userModel))
                return ($userModel->delete())? '' : ['error'=> 'internal error'];
            else
                return ['error'=> 'user not found'];

        }catch (\Exception $e ){
            return ['error'=> 'internal error'];
        }
    }

}
