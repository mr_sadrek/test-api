<?php

namespace frontend\models;

use Abraham\TwitterOAuth\TwitterOAuth;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "publications".
 *
 * @property int $id
 * @property string $twitter_publication_id
 * @property string $user
 * @property string $tweet
 * @property string $hashtag
 * @property int $created_at
 */
class Publications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'publications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'created_at', ], 'integer'],
            [['tweet','hashtag','user','twitter_publication_id'], 'string'],
            [['twitter_publication_id'],'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('publications', 'ID'),
            'user' => Yii::t('publications', 'Twitter User'),
            'created_at' => Yii::t('publications', 'Created At'),
            'twitter_publication_id' => Yii::t('publications', 'Twitter Publication ID'),
            'tweet' => Yii::t('publications', 'Text'),
            'hashtag' => Yii::t('publications', 'hashtag'),
        ];
    }

    public function getTweets ($user){
        $tokens = array("consumer_key" => "nI5mbQJ90srwBDIPGBrAIUphJ", "consumer_key_secret" => "EMubqMnZNuIIy6echinNa7uXd3xM67jfyZ6SmPiiXkUUdcCApb", "access_token" => "38682087-3RjO1RdEQpAmnt5rzwhiFiWKsCACQKY1zHfPYpAkF", "access_token_secret" => "gl4WAIpTRSFnld6kJxaD68pcn9IyEEJzGQO4z7r3TR1zm");
        $connection = new TwitterOAuth($tokens["consumer_key"], $tokens["consumer_key_secret"], $tokens["access_token"], $tokens["access_token_secret"]);

        $content = $connection->get('statuses/user_timeline',['screen_name'=>$user,]);
        if(empty($content->errors)){
            foreach ($content as $i=>$tweet){

                $model = new Publications();

                $model->tweet = self::mysql_utf8_sanitizer($tweet->text);
                $model->twitter_publication_id = $tweet->id_str;
                $model->user = $tweet->user->screen_name;
                $model->created_at = strtotime($tweet->created_at);
                if(!empty($tweet->entities->hashtags)) {
                    $model->hashtag = '';
                    foreach ($tweet->entities->hashtags as $hashtag){
                        $model->hashtag .=  (strlen($model->hashtag) == 0)? $hashtag->text . '; ' : $hashtag->text . '; ' ;
                    }
                }
                if($model->validate()){
                    $model->save();
                }else{
                    continue;
                }
            }
        }


    }


    // Modified from: https://stackoverflow.com/a/24672780/2726557
    function mysql_utf8_sanitizer(string $str)
    {
        return preg_replace('/[\x{10000}-\x{10FFFF}]/u', "\xEF\xBF\xBD", $str);
    }
}
