<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Publications;

/**
 * PublicationsSearch represents the model behind the search form of `frontend\models\Publications`.
 */
class PublicationsSearch extends Publications
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'twitter_publication_id', 'created_at'], 'integer'],
            [['user', 'tweet', 'hashtag'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Publications::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'twitter_publication_id' => $this->twitter_publication_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'user', $this->user])
            ->andFilterWhere(['like', 'tweet', $this->tweet])
            ->andFilterWhere(['like', 'hashtag', $this->hashtag]);

        return $dataProvider;
    }
}
