<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\UserList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-list-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'twitter_user_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('user_list', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
